---
layout: page
title: About
permalink: /about/
---

# What is AfricaOnRails?
AfricaOnRails is an initiative to make programming and programming lessons available to everyone. Programming is a great way to become financially stable and we want to give everyone the opportunity to achieve this.

# What do we offer?
We offer regular teaching sessions via webconferencing and free subscriptions to resources to learn to program.

# Who can apply?
Anyone can apply - you will need a computer with administrator access, internet connection, webcam and microphone, you will need to be able to speak and understand English.

# Who are we?
Sytse - programmer and software architect, entrepreneur and cofounder of GitLab.com.
Karen - programmer and boardmember of RailsGirls NL, an initiative to introduce Dutch women to programming and Rails.
